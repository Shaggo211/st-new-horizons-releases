# This is a template which multiple species can use. 
  
humanoid_07_female_hair_01 = {
	default = "gfx/models/portraits/all/bald.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/all/bald.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/all/bald.dds"
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/all/bald.dds"
	}
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/all/bald.dds"
	}
  
	#leader scope 
	ruler = { #for rulers
		default = "gfx/models/portraits/all/bald.dds"
	}
}
  