#Random List for Core Stars in the Alternative Galaxy
random_list = {
	name = "core_star_b"
	regular_a_b = 5
	regular_b_b = 5
	regular_f_b = 5
	regular_g_b = 5
	regular_k_b = 5
	regular_m_b = 5	
	black_b = 5
	neutron_b = 5
	pulsar_b = 5
}

#Random Lists for Particular Stars

random_list = {
	name = "rl_alpha_centauri"
	alpha_centauri_a = 10
	alpha_centauri_b = 10
	scg_b = 2
}

random_list = {
	name = "rl_sirius"
	sirius = 10
	sirius_b = 10
	sca_b = 2
}

random_list = {
	name = "rl_vega"
	vega = 10
	vega_b = 10
	sca_b = 2
}

random_list = {
	name = "rl_eta_cassiopeiae"
	terra_nova = 10
	terra_nova_b = 10
	scg_b = 2
}

random_list = {
	name = "rl_epsilon_eridani"
	axanar = 10
	axanar_b = 10
	sca_b = 2
}

random_list = {
	name = "rl_epsilon_ceti"
	risa = 10
	risa_b = 10
	scf_b = 2
}

random_list = {
	name = "rl_denobula_triaxa"
	denobula_triaxa_b = 10
	scg_b = 10
}

random_list = {
	name = "rl_zeta_bootis"
	zeta_bootis_b = 10
	scf_b = 5
}

random_list = {
	name = "rl_capella"
	capella_b = 10
	sck_b = 5
}

random_list = {
	name = "rl_deneb_kaitos"
	deneb_kaitos_b = 10
	sck_b = 10
}

random_list = {
	name = "rl_epsilon_indi"
	epsilon_indi_b = 10
	sck_b = 5
}

random_list = {
	name = "rl_tau_ceti"
	tau_ceti_b = 10
	scg_b = 5
}

random_list = {
	name = "rl_altair"
	altair_b = 10
	sca_b = 10
}

random_list = {
	name = "rl_70_ophiuchi"
	70_ophiuchi_b = 10
	sck_b = 5
}

random_list = {
	name = "rl_sigma_draconis"
	sigma_draconis_b = 10
	scg_b = 5
}

random_list = {
	name = "rl_coridan"
	coridan_b = 10
	sca_b = 5
}

random_list = {
	name = "rl_ventax"
	ventax_b = 10
	scg_b = 5
}

random_list = {
	name = "rl_tellun"
	tellun_b = 10
	scg_b = 5
}