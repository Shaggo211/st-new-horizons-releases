
borg_collective = {
	has_difficulty_bonuses = yes
	counts_for_victory = yes
	playable = yes
	shuffle_pop_ethos = no
	sub_title = "The Collective"
	sub_title_desc = "A genocidal race of cybernetic organisms bent on assimilating the galaxy in their quest for 'perfection'."
	diplomatic_wars = no
	relations = no
	custom_diplomacy = yes	


	ai = {
		enabled = yes
		declare_war = yes
		colonizer = yes
		internal_policies = yes
		construction = yes
		tech = yes
		modules = {
			military_minister_module
			foreign_minister_module
			interior_minister_module
		}
		
		ship_data = {
			constructor = {
				min = 1
				max = 3
				planet_mult = 0.25
			}
			
			science	= {
				min = 1
				max = 2
				planet_mult = 0.5
			}
			
			colonizer = {
				min = 1
				max = 2
			}
			
		
		
			corvette = {
				fraction = {
					base = 100
					modifier = {
						add = -25
						has_technology = tech_spaceport_3
					}	
					modifier = {
						add = -45
						has_technology = tech_spaceport_5
					}
					modifier = {
						add = -5
						has_technology = tech_spaceport_6
					}						
				}
			}

			saber = {
				fraction = {
					base = 100
					modifier = {
						add = -25
						has_technology = tech_spaceport_3
					}	
					modifier = {
						add = -45
						has_technology = tech_spaceport_5
					}
					modifier = {
						add = -5
						has_technology = tech_spaceport_6
					}						
				}
			}

			sovereign = {
				fraction = {
					base = 100
					modifier = {
						add = -25
						has_technology = tech_spaceport_3
					}	
					modifier = {
						add = -45
						has_technology = tech_spaceport_5
					}
					modifier = {
						add = -5
						has_technology = tech_spaceport_6
					}						
				}
			}

			steamrunner = {
				fraction = {
					base = 100
					modifier = {
						add = -25
						has_technology = tech_spaceport_3
					}	
					modifier = {
						add = -45
						has_technology = tech_spaceport_5
					}
					modifier = {
						add = -5
						has_technology = tech_spaceport_6
					}						
				}
			}

		}		
		army_data = {
			defense_army = {
				fraction = {
					factor = 50
					modifier = {
						add = 10
						OR = {
							has_ethic = ethic_pacifist
							has_ethic = ethic_fanatic_pacifist
						}
					}
				}
			}
			
			assault_army = {
				fraction = {
					factor = 50
					modifier = {
						add = -20
						OR = {
							AND = {
								has_ai_personality_behaviour = slaver
								has_technology = tech_neural_implants
							}
							AND = {
								has_ai_personality_behaviour = robot_exploiter
								has_technology = tech_droid_workers
							}
						}
					}
					modifier = {
						add = -10
						OR = {
							has_ethic = ethic_pacifist
							has_ethic = ethic_fanatic_pacifist
						}
					}
					modifier = {
						factor = 0
						OR = {
							has_technology = tech_cloning
							has_technology = tech_telepathy
							has_technology = tech_synthetic_workers
							has_technology = tech_gene_seed_purification
							AND = {
								exists = ruler
								ruler = { 
									has_leader_flag = has_elite_guard 
								}
								OR = {
									has_government = despotic_hegemony
									has_government = ai_overlordship
								}
							}
						}
					}
				}
			}
			
			slave_army = {
				fraction = {
					factor = 20
					modifier = {
						factor = 0
						OR = {
							NOT = {
								AND = {
									has_ai_personality_behaviour = slaver
									has_technology = tech_neural_implants
								}
							}
							AND = {
								has_ai_personality_behaviour = robot_exploiter
								has_technology = tech_droid_workers
							}
						}
					}
				}
			}
			
			robotic_army = {
				fraction = {
					factor = 20
					modifier = {
						factor = 0
						NOT = {
							has_ai_personality_behaviour = robot_exploiter
							has_technology = tech_droid_workers
						}
					}
				}
			}
			
			clone_army = {
				fraction = {
					factor = 50
					modifier = {
						add = -20
						OR = {
							AND = {
								has_ai_personality_behaviour = slaver
								has_technology = tech_neural_implants
							}
							AND = {
								has_ai_personality_behaviour = robot_exploiter
								has_technology = tech_droid_workers
							}
						}
					}
					modifier = {
						add = -10
						OR = {
							has_ethic = ethic_pacifist
							has_ethic = ethic_fanatic_pacifist
						}
					}
					modifier = {
						factor = 0
						OR = {
							NOT = {
								has_technology = tech_cloning
							}
							has_technology = tech_telepathy
							has_technology = tech_synthetic_workers
							has_technology = tech_gene_seed_purification
							AND = {
								exists = ruler
								ruler = { 
									has_leader_flag = has_elite_guard
								}
								OR = {
									has_government = despotic_hegemony
									has_government = ai_overlordship
								}
							}
						}
					}
				}
			}
			
			psionic_army = {			
				fraction = {
					factor = 50
					modifier = {
						add = -20
						OR = {
							AND = {
								has_ai_personality_behaviour = slaver
								has_technology = tech_neural_implants
							}
							AND = {
								has_ai_personality_behaviour = robot_exploiter
								has_technology = tech_droid_workers
							}
						}
					}
					modifier = {
						add = -10
						OR = {
							has_ethic = ethic_pacifist
							has_ethic = ethic_fanatic_pacifist
						}
					}
					modifier = {
						factor = 0
						OR = {
							NOT = {
								has_technology = tech_telepathy
							}
							has_technology = tech_synthetic_workers
							has_technology = tech_gene_seed_purification
							AND = {
								exists = ruler
								ruler = { 
									has_leader_flag = has_elite_guard
								}
								OR = {
									has_government = despotic_hegemony
									has_government = ai_overlordship
								}
							}
						}
					}
				}
			}
			
			android_army = {
				fraction = {
					factor = 50
					modifier = {
						add = -20
						OR = {
							AND = {
								has_ai_personality_behaviour = slaver
								has_technology = tech_neural_implants
							}
							AND = {
								has_ai_personality_behaviour = robot_exploiter
								has_technology = tech_droid_workers
							}
						}
					}
					modifier = {
						add = -10
						OR = {
							has_ethic = ethic_pacifist
							has_ethic = ethic_fanatic_pacifist
						}
					}
					modifier = {
						factor = 0
						OR = {
							NOT = {
								has_technology = tech_synthetic_workers
							}
							has_technology = tech_gene_seed_purification
							AND = {
								exists = ruler
								ruler = {
									has_leader_flag = has_elite_guard
								}
								OR = {
									has_government = despotic_hegemony
									has_government = ai_overlordship
								}
							}
						}
					}
				}
			}
			
			gene_warrior_army = {
				fraction = {
					factor = 50
					modifier = {
						add = -20
						OR = {
							AND = {
								has_ai_personality_behaviour = slaver
								has_technology = tech_neural_implants
							}
							AND = {
								has_ai_personality_behaviour = robot_exploiter
								has_technology = tech_droid_workers
							}
						}
					}
					modifier = {
						add = -10
						OR = {
							has_ethic = ethic_pacifist
							has_ethic = ethic_fanatic_pacifist
						}
					}
					modifier = {
						factor = 0
						OR = {
							NOT = {
								has_technology = tech_gene_seed_purification
							}
							AND = {
								exists = ruler
								ruler = { 
									has_leader_flag = has_elite_guard
								}
								OR = {
									has_government = despotic_hegemony
									has_government = ai_overlordship
								}
							}
						}
					}
				}
			}
			
			elite_guard_army = {
				fraction = {
					factor = 50
					modifier = {
						add = -20
						OR = {
							AND = {
								has_ai_personality_behaviour = slaver
								has_technology = tech_neural_implants
							}
							AND = {
								has_ai_personality_behaviour = robot_exploiter
								has_technology = tech_droid_workers
							}
						}
					}
					modifier = {
						add = -10
						OR = {
							has_ethic = ethic_pacifist
							has_ethic = ethic_fanatic_pacifist
						}
					}
					modifier = {
						factor = 0
						exists = ruler
						NOT = {
							ruler = { 
								has_leader_flag = has_elite_guard
							} 
							OR = {
								has_government = despotic_hegemony
								has_government = ai_overlordship
							}
						}
					}
				}
			}
		}
	}
	faction = {
		needs_colony = yes
		hostile = yes
		needs_border_access = no
	}
	modules = {
		standard_event_module = {}
		standard_economy_module = {}
		standard_leader_module = {}
		exclusive_diplomacy_module = {
			can_receive = {
				action_declare_war
				action_offer_peace
				action_make_rival
				action_end_rivalry
				action_insult
			}
			can_send = {
				action_declare_war
				action_offer_peace
			}
		}
		standard_technology_module = {}
}

