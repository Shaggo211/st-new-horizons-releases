### Basic Resources

sr_yridium_bicantizine = {
	AI_category = strategic_resource
	prerequisites = { "tech_ketracel_white_synthesis" }
	tradable = yes
	is_rare = yes
}

sr_sodium_tetrahydrate = {
	AI_category = strategic_resource
	prerequisites = { "tech_ketracel_white_synthesis" }
	tradable = yes
	is_rare = yes
}

ketracel_white = {
	accumulative = yes
	collect_if_wrong_building = yes
	accumulated_by_sectors = yes
	tradable = yes
	AI_category = strategic_resource
	max = 200
	prerequisites = { "tech_ketracel_white_synthesis" }
	is_rare = yes
}