### Ferengi Alliance
Ferengi = {
	randomized = no

	ship_names = {
		generic = {
			D'Kora Nagus Nandi Fiscal Pareto Credit Marauder Abundance Acquisition Actuary Adeel Affidavit Affluence Allotment Alltier Amortize Andrion Annex Annuity Appraiser Arbitrage Arbitration Arrears Asin Assay Assessor Auditor Avarice Avonia Bestan Bokira Bovris Braktel Capitalist Chulebas Climmakral Client Commerce Commission Company Compound Contrarian Contribution Credit Crell Dalion Deyer Domu Dussooron Edja Krannek Garvan Hartan Hester Himpos Igen Karluk Keechvar Kekron Kimel-Mirom Korr Kraalor Kramora Krayton Krayvis Kreechta Kyrasam "Latinum Queen" "Leveraged Buyout" Margin Merchant Merger Mes-Vatra Moori Nam-Breth Negotiator Notary Ob-Strori Oetalgolor Ondik Preekon Qartum Rard-Pis "Rational Profit" "Seventy-Fifth Rule" Soodii Tadinia Tampanium Tenyt Tinnarr Togram Usotroo Varkart Venture Wealth Windfall
		}		
		orbital_station = { }
		mining_station = { }
		research_station = { }
		wormhole_station = { }
		terraform_station = { }
		observation_station = { }
		outpost_station = {
			sequential_name = "%O% Frontier Outpost"
		}
	}

	fleet_names = {
		sequential_name = "%O% Privateers"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Domestic Guard"
		}
		assault_army = {
			sequential_name = "%O% Mauraders"
		}
		slave_army = {
			sequential_name = "%O% Indentured Phasers"
		}
		clone_army = { 
			sequential_name = "%O% Clone Mauraders"
		}
		robotic_army = {
			sequential_name = "%O% Mechanical Mauraders"
		}
		android_army = {
			sequential_name = "%O% Synthetic Mauraders"
		}
		psionic_army = { 
			sequential_name = "%O% Psi Mauraders"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Mauraders"
		}
		gene_warrior_army = {
			sequential_name = "%O% Bio-Engineered Mauraders"
		}
	}

	planet_names = {
		generic = {
			names = {
				"New Ferenginar" Clarus Irtok Lappa Volchok
			}
		}
	}


	### CHARACTERS

	character_names = {
		names1 = {
			weight = 100
			first_names_male = {
				Adred Arridor Barbo Belongo Berik Birta Bok Bractor Brak Broik Brunt Farek Frin Frool Gaila Gant Gegis Gint Gorad Goss Gral Grimp Grish Hoex Igel Kayron Kazago Keldar Kol Kono Krax Krem Kro Leck "Par Lenor" Letek Lonzo Lurin Mordoc Morta Muk Nava Nibor Nilva Nog Nunk Omag Orpax Plegg Prak Qol Quark Rata Reyga Rom Smeet Solok Sovak Stol Tarr Tog Tol Torrot Turot Tye Ulis Yeggie Zek 
			}
			first_names_female = {
				Ishka Pel Prinadora Lumba Ishkadora Lumbadora Peladora Prin Nisha Qolshka Frolshka Frinadora Gel Beladora Bel Grishka Koladora Arridora Hoshka Orshka Kazshka Rey Sol Sov Sto Stoladora Let Gin Bir Bractadora Birshka
			}
			second_names = {
				" "
			}
			regnal_first_names_male = {
				Adred Arridor Barbo Belongo Berik Birta Bok Bractor Brak Broik Brunt Farek Frin Frool Gaila Gant Gegis Gint Gorad Goss Gral Grimp Grish Hoex Igel Kayron Kazago Keldar Kol Kono Krax Krem Kro Leck "Par Lenor" Letek Lonzo Lurin Mordoc Morta Muk Nava Nibor Nilva Nog Nunk Omag Orpax Plegg Prak Qol Quark Rata Reyga Rom Smeet Solok Sovak Stol Tarr Tog Tol Torrot Turot Tye Ulis Yeggie Zek 
			}
			regnal_first_names_female = {
				Ishka Pel Prinadora Lumba Ishkadora Lumbadora Peladora Prin Nisha Qolshka Frolshka Frinadora Gel Beladora Bel Grishka Koladora Arridora Hoshka Orshka Kazshka Rey Sol Sov Sto Stoladora Let Gin Bir Bractadora Birshka				
			}
			regnal_second_names = {
				" "
			}
		}
	}
}