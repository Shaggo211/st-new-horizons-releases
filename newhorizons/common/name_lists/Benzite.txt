﻿### Benzite
Benzite = {
	randomized = no

	ship_names = {
		generic = {
			Gelkot Andross Belross Bepsot Belross Chandock Panzor Galmut Chadock Dathet Entoss Entock Gelkoss Gelkot Grevan Gral Farkoss Freptas Lartak Lorveth Lustoss Mantoss Methot Mordoss Morneth Panthak Renas Entock Gelkot Grevan Ker Mantoss Mordoss Panthoss Renas Toruk Tarkat Tarkoss Tekross Tekra Tatock Tatoss Toruk Wipvoss Wipvuk Utva Pheradon Merria Herti Dwora
		}
		corvette = { }
		constructor = { }
		colonizer = { }
		science = { }
		destroyer = { }
		cruiser = { }
		battleship = { }
		transport = { }
		orbital_station = { }
		mining_station = { }
		research_station = { }
		wormhole_station = { }
		terraform_station = { }
		observation_station = { }
		outpost_station = {
			sequential_name = "%O% Frontier Outpost"
		}
	}

	fleet_names = {
		sequential_name = "%O% Fleet"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Benzite Guard"
		}
		assault_army = {
			sequential_name = "%O% Benzite Army"
		}
		slave_army = {
			sequential_name = "%O% Indentured Defence Force"
		}
		clone_army = { 
			sequential_name = "%O% Clone Defence Force"
		}
		robotic_army = {
			sequential_name = "%O% Mechanical Defence Force"
		}
		android_army = {
			sequential_name = "%O% Synthetic Defence Force"
		}
		psionic_army = { 
			sequential_name = "%O% Psi Defence Force"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Defence Force"
		}
		gene_warrior_army = {
			sequential_name = "%O% Bio-Engineered Defence Force"
		}
	}

	planet_names = {
		generic = {
			names = {
			}
		}
	}


	### CHARACTERS

	character_names = {
		names1 = {
			weight = 40
			first_names_male = {
			Mordock Laporin Mendon Oorv Pirk Gari Culer Nenap Canut Huro Yunnel Pizann Paluh Curak Roriy Gihen Dobet Mosap Tined Polik Gazed Dupey Cripbar Bamiz Marin Hoydock Mendock Dolin Sabin Cirdock Hobock Cecin Gapin Sohin Gizin Pizin
			}
			first_names_female = {
			Marya Hoya Nonin Dolah Sabah Cira Hubo Ceza Cecir Gapy Sohiz Gizy Piza Nuzza Kidii Mecia Tonuo Sinuo Busia Rucez Zukel Canoh Dezako Zoni Yunna Morda Lapora Menda Oorva Pirkah Gara Culah Nenah Canuh Hury Yunny Pizah Palah Curah Rorah Gihah Dobor Mosir
			}
			second_names = {
				" "
			}
			regnal_first_names_male = {
			Mordock Laporin Mendon Oorv Pirk Gari Culer Nenap Canut Huro Yunnel Pizann Paluh Curak Roriy Gihen Dobet Mosap Tined Polik Gazed Dupey Cripbar Bamiz Marin Hoydock Mendock Dolin Sabin Cirdock Hobock Cecin Gapin Sohin Gizin Pizin	
			}
			regnal_first_names_female = {
			Marya Hoya Nonin Dolah Sabah Cira Hubo Ceza Cecir Gapy Sohiz Gizy Piza Nuzza Kidii Mecia Tonuo Sinuo Busia Rucez Zukel Canoh Dezako Zoni Yunna Morda Lapora Menda Oorva Pirkah Gara Culah Nenah Canuh Hury Yunny Pizah Palah Curah Rorah Gihah Dobor Mosir
			}
			regnal_second_names = {
				" "
			}
		}
	}
}