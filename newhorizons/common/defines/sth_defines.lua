--[[
NDefines = {	


	NGameplay = {
		FTL_RANGE_JUMPDRIVE		= 40,
	}
} 
]]--

NDefines.NInterface.BROWSER_BASE_URL	= "http://www.stellariswiki.com/StarTrek_NewHorizons"
NDefines.NGameplay.FTL_RANGE_WARP 								= 40	-- Base of warp is used for transwarp. All normal warp drives should start with a large range multiplier
NDefines.NGameplay.FTL_RANGE_JUMPDRIVE							= 40
NDefines.NGameplay.SHIP_SUBLIGHT_SPEED_MULT						= 12	-- General speed multiplier for ship movement within solar systems -- STH: Increased to reduce wasting time
NDefines.NGameplay.MISSILE_SUBLIGHT_SPEED_MULT 					= 10	-- General speed multiplier for ship movement within solar systems -- STH: Faster = better?
NDefines.NGameplay.SCIENCE_SHIP_WORK_SPEED_MULT 				= 1.3
NDefines.NGameplay.LEADER_POOL_SIZE								= 5		-- Each leader pool will consist of this many leaders -- STH: Increased to reduce randomness
NDefines.NGameplay.LEADER_POOL_LEAD_TIME						= 5		-- Years leaders will remain in the leader pool until replaced -- STH: Decreased to reduce randomness
NDefines.NGameplay.LEADER_SKILL_CAP								= 6
NDefines.NGameplay.LEADER_EXPERIENCE_REQUIREMENT_SCALE_VALUE	= 100	-- Scale factor when calculating required experience for next skill level -- STH: Increased to make higher level leaders more rare
NDefines.NGameplay.LEADER_SCIENTIST_ASSIST_RESEARCH_MULT		= 0.75	-- Assist Research modifier-mult per skill level (this value times skill level equals modifier) -- STH: Effect reduced... too powerful
NDefines.NGameplay.BORDER_POPULATION_MODIFIER					= 0.20	-- The border population modifier -- STH: Increased to represent megapolis-planets causing large borders
NDefines.NGameplay.BORDER_BASE_RADIUS							= 8		-- The base radius of the border
NDefines.NGameplay.WAR_SCORE_BLOCKADE_MUL						= 0.20	-- Percentage warscore gained from having planets blockaded instead of occupied. -- STH: Changed from 0.25
NDefines.NGameplay.WAR_SCORE_SHIP_KILL_MUL 						= 0.15	-- Multiplier of warscore gained -- STH: Changed from 0.25 (way more ships blowing up in STH due to construction speed)
NDefines.NGameplay.WAR_SCORE_SHIP_KILL_WINNER_MUL				= 0.60	-- Multiplier of warscore loss for the sinking of ships for the winner -- STH: Changed from 0.75
NDefines.NGameplay.WAR_SCORE_PLANET_BASE						= 15.0	-- Relative warscore value of a planet -- STH: Taking planets harder, but more rewarding (changed from 10)
NDefines.NGameplay.WAR_SCORE_PLANET_BUILDING					= 1.5	-- Relative warscore value of a planet for every building -- STH: From 1.0
NDefines.NGameplay.WAR_SCORE_OCCUPATION_MUL						= 2.0	-- 150% warscore gained from completely occupying the opponents -- STH: From 1.5
NDefines.NGameplay.WAR_SCORE_PLANET_MIN 						= 10.0	-- Warscore from 1 planet will never be less than this -- STH: From 5
NDefines.NGameplay.WAR_SCORE_WARGOAL_MULT						= 1.75	-- Wargoal planets are worth this much more -- STH: MUCH more emphasis on wargoals (from 1.25)
NDefines.NGameplay.GARRISON_SIZE_BASE							= 3		-- Planet always spawns this number of garrisons -- STH: From 2
NDefines.NGameplay.GARRISON_SIZE_MULT							= 0.6	-- Per pop over 1 -- STH: From 0.4
NDefines.NGameplay.PLANET_FORTIFICATION_DEFENSE_MIN				= 1.10	-- At 0% fortification, divide damage to defending armies by this -- STH: From 0.5... defenders retain an advantage even at 0% fort -- ground wars harder and more costly
NDefines.NGameplay.DEBRIS_ANALYZED_TECH_PROGRESS				= 0.25	-- Tech progress added when analyzing component that require tech you don't have -- STH: Brought up from 0.10 - much easier to aquire tech this way
NDefines.NGameplay.CUSTOM_EMPIRE_SPAWN_CHANCE 		        	= 1000	-- Chance that an empire will be created from an existing template instead of randomly generated (10 = 1% chance)
NDefines.NGameplay.GROUND_SUPPORT_FULL_DMG						= 0.60	-- Multiplier to fortification damage -- STH: Increased due to requiring special policies
NDefines.NGameplay.NUM_TECH_ALTERNATIVES						= 3		-- STH: decreased to 3 again
NDefines.NGameplay.TECH_COST_MULT_NUM_COLONIES					= 0.07	-- Each Planet adds +X% tech cost 	--STH: Changed from 0.1
NDefines.NGameplay.DOCKED_FLEET_MAINTENANCE_REDUCTION			= 0.40	-- Docked fleets will have a lowered maintenance reduction of this amount -- STH: Increased from 0.25 - decision on when to use ships should be a very important decision
NDefines.NGameplay.SECTOR_SUPPORT_RESOURCE_AMOUNT				= 200 	-- How much resources each click will give the sector. -- STH: Doubled to reduce clicking
NDefines.NGameplay.RESEARCH_AGREEMENT_COST_MULT					= -0.35	-- Technology cost reduced with 25% for trade research agreement deal  -- STH: Changed from -0.25
NDefines.NGameplay.SUBJECT_INTEGRATION_BASE_PROGRESS 			= 10	-- Cost per month for subject integration -- STH: Doubled progress to speed things up
NDefines.NGameplay.SUBJECT_INTEGRATION_COST_PER_POP 			= 2.5 	-- Integration cost per pop  -- STH: Cut in half - taking years and years sucks
NDefines.NGameplay.SUBJECT_INTEGRATION_COST_PER_PLANET			= 25 	-- Integration cost per planet -- STH: Cut in half
NDefines.NGameplay.MAX_FRICTION 								= 50	-- Max friction (total)
NDefines.NGameplay.FRICTION_FROM_BORDERING_SYSTEM 				= 1.0	-- Friction for each bordering system
NDefines.NGameplay.COLONY_SENSOR_RANGE 							= 20.0	-- Base Sensor range from system from having colony in it (does not stack) -- STH: changed from 40 to 20 

NDefines.NGameplay.STARTING_WEAPON_TECHS = {
	"tech_phaser_cannon_1",
	"tech_disruptor_cannon_1",
	"tech_plasma_cannon_1",
	"tech_tetryon_cannon_1",
	"tech_polaron_cannon_1",
	"tech_antiproton_beam_1",
	--"tech_mass_drivers_1",
	--"tech_lasers_1",
	--"tech_missiles_1",
}
NDefines.NGameplay.STARTING_WEAPON_TECHS_ICONS = {
	"GFX_weapon_type_phaser",
	"GFX_weapon_type_disruptor",
	"GFX_weapon_type_plasma",
	"GFX_weapon_type_tetryon",
	"GFX_weapon_type_polaron",
	"GFX_weapon_type_antiproton",
	--"GFX_weapon_type_01",
	--"GFX_weapon_type_02",
	--"GFX_weapon_type_03",
}
NDefines.NGameplay.STARTING_FTL_TYPES = {
	"warp",
	--"hyperdrive",
	--"wormhole",
}

--NDefines.NGameplay.STARTING_FTL_TYPES = { -- STH: Players can ONLY start with warp
--	"warp",
--}

NDefines.NGraphics.BALLISTIC_PROJECTILE_MISSED_LIFETIME     = 5.0 		-- missed ballistic projectiles will live for (at least) this long before being removed
NDefines.NGraphics.MISSED_BEAM_LENGTH                       = 1000.0 	-- 500.0 how long missed beams will be
NDefines.NGraphics.MISSILE_HEIGHT_OFFSET	            	= -5.0		-- how high up missiles will fly
NDefines.NGraphics.MISSILE_ROTATION_RATE	            	= 10.0 		-- how fast missiles will adjust their rotation. High values can cause visible snaps each micro update
NDefines.NGraphics.MISSILE_RANDOM_OFFSET_MIN_RADIUS         = 2.5		-- random spread of missiles
NDefines.NGraphics.MISSILE_RANDOM_OFFSET_MAX_RADIUS         = 5.0
NDefines.NGraphics.SHIP_RANDOM_HEIGHT_OFFSET                = 40.0 		-- 20.0


NDefines.NShip.DESIGNER_WEAPON_STACKING_DIV					= 0.15		-- The higher this is, the more the ship designer will try to vary which weapon types are used on its sections -- STH: Reduced from 0.2
NDefines.NShip.ENERGY_MAINTENANCE_MUL						= 0.0115	-- Ship Maintenance Multiplier, Percentage of the cost of the ship -- STH: Increased by 50% - make maintenance costs of ships much more important
NDefines.NShip.MINERAL_MAINTENANCE_MUL						= 0.0115	-- Ship Maintenance Multiplier, Percentage of the cost of the ship -- STH: used to be 0.007ish
NDefines.NShip.FLEET_BASE_FORMATION_SCALE					= 1.5 		-- STH: Formations kept tighter together (more cinematic)
NDefines.NShip.WARP_WINDUP 									= 1.5 		-- Distance multiplier to warp windup in microupdates -- STH: Increased by 50%
NDefines.NShip.WARP_WINDDOWN 								= 2.0 		-- Distance multiplier to warp winddown in microupdates -- STH: Reduced from 5.5 (Warp drives in Star Trek are flexible)
NDefines.NShip.WARP_INTERSTELLAR_TRAVEL_SPEED 				= 0.08	 	-- In micro updates ( 10/day ) -- STH: Cut in half.. feels pretty slow, but doesn't make things crawl




NDefines.NCombat.COMBAT_EMERGENCY_FTL_PENALTY_HITPOINTS 	= 0.15 		-- x% of max hitpoints in penalty for doing ftl jump -- STH: Reduced from 0.25... encourage breaking off and engaging elsewhere
NDefines.NCombat.COMBAT_EMERGENCY_FTL_SURVIVE_CHANCE 		= 0.50 		-- x% chance to survive with x% health even if ship should have died -- STH: Increased from 0.1 - encourage emergency FTLs
NDefines.NCombat.FLEET_MOVE_MAX_DIST_SHIPS                  = 50.0		-- 10.0 Max distance that ships are allowed to travel before last ship


NDefines.NEconomy.COLONY_SHIP_MAINTENANCE 					= 4.0 		-- Monthly colony ship maintenance -- STH: Doesn't make sense that colony ship main = colony maint.. cut in half
NDefines.NEconomy.FLEET_UPGRADE_TIME_COST_MULT				= 0.25		-- Upgrade fleet cost -- STH: reduced from 0.75 - upgrading a ship *much* faster than creating it from scratch
NDefines.NEconomy.EXPANSION_COST_DISTANCE_COST				= 3.0		-- each 1 range equals X of this define as an influence cost to colonize or build a frontier outpost -- STH: Increased from 2 to discourage long-range colonies
NDefines.NEconomy.EXPANSION_COST_DISTANCE_SCALE 			= 1.15		-- scaled cost for exponentially increased costs depending on range -- STH: Increased to discourage long-range colonies (border gore)


NDefines.NAI.THREAT_PLANET_MULT								= 8			-- Base threat multiplier for planet -- STH increased from 5
NDefines.NAI.THREAT_POP_MULT								= 0.45		-- Base threat multiplier for pop -- STH increased from 0.33
NDefines.NAI.DIPLOVASSALIZE_THREAT                          = 1.8		-- Threat generated from diplovassalization (scales with size of vassalized empire) -- STH increased from 1.5
NDefines.NAI.SHARED_THREAT_MULT                             = 1.0		-- How much does having a shared threat boost opinion? -- STH changed from 0.5
NDefines.NAI.THREAT_DISTANCE_FACTOR                         = 0.005		-- The higher this is, the larger the threat reduction from being far away -- STH changed from 0.01
NDefines.NAI.THREAT_SIZE_FACTOR                             = 1.6		-- How much does the relative power of the aggressive empire affect threat? -- STH changed from 1
NDefines.NAI.THREAT_SIZE_FACTOR_MAX                         = 3.0		-- STH changed from 2
NDefines.NAI.THREAT_POSITIVE_OPINION_FACTOR                 = 0.003		-- How much does opinion of the victim affect threat? -- STH changed from 0.002
NDefines.NAI.THREAT_NEGATIVE_OPINION_FACTOR                 = 0.0035	-- STH changed from 0.002 (threat from people we hate is handled seriously)
NDefines.NAI.THREAT_OPINION_MAX                             = 1.50		-- STH - widened the gap from 1.25
NDefines.NAI.ALLIANCE_ACCEPTANCE_SHARED_RIVAL               = 25		-- per rival # STH: From 10, encourage alliances against rivals
NDefines.NAI.ALLIANCE_ACCEPTANCE_SHARED_THREAT              = 0.7		-- scales with actual threat # STH: From 0.5... allies react more strongly to threat
NDefines.NAI.VASSALIZATION_ACCEPTANCE_PROTECTORATE_MAX      = 40		-- STH: Increased from 20.. becoming a protectorate isn't a bad thing		
NDefines.NAI.VASSALIZATION_ACCEPTANCE_NUM_POPS              = -0.25		-- per pop -- STH: Reduced from -0.50. Population doesn't mean very much... if an extremely powerful empire wants to vassalize you, you would rather avoid genocide, right?
NDefines.NAI.VASSALIZATION_ACCEPTANCE_POWER_DIFFERENCE_MULT = 10  		-- for each 1x over min -- STH: Vastly more powerful empires are very attractive to vassalize under
NDefines.NAI.VASSALIZATION_ACCEPTANCE_POWER_DIFFERENCE_MAX  = 150		-- STH: From 100
NDefines.NAI.VASSALIZATION_ACCEPTANCE_ATTITUDE_ALLIANCE     = 20		-- STH: Already in an alliance? Why not vassalize..
NDefines.NAI.VASSALIZATION_ACCEPTANCE_OTHER_ATTITUDE        = -60		-- STH: Other attitude is fine but if the enemy empire is extremely powerful... 
NDefines.NAI.OFFER_VASSALIZATION_ACCEPTANCE_OTHER_ATTITUDE  = -150 		-- STH: Changed from -1000.. if a vastly more powerful empire wants to vassalize you.. better than genocide
NDefines.NAI.FEDERATION_ACCEPTANCE_SHARED_THREAT            = 3			-- STH: Brought up from 2
NDefines.NAI.STATION_BUDGET_FRACTION                        = 0.30 		-- AI will spend this fraction of their income on stations -- STH: Moved to creating robots
NDefines.NAI.ROBOT_BUDGET_FRACTION                          = 0.15		-- AI will spend this fraction of their income on robots (transfered to stations & buildings if they don't use robots) -- STH: Increased, robots are good
NDefines.NAI.COLONIZER_SHIPS_MAX                            = 2			-- Maximum number of colonizer ships of AI -- STH: Increased to 2... in STH, colonies take longer to build out
NDefines.NAI.RESEARCH_SHIPS_MAX                             = 3			-- Maximum number of research ships of AI -- STH: Increased - anomolies more important
NDefines.NAI.START_WEAPON_TECH_AREA_MULT                    = 3			-- Weapons within the same tech area as the start weapon will be weighted (multiplied) by this value when deciding tech strategies -- STH: Increase from 2

NDefines.NGameplay.START_YEAR                               = 2150		-- KEEP THIS HERE!




