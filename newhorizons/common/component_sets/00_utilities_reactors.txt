# Icons set here are the ones shown in compnent editor list.

component_set = {
	key = "FUSION_REACTOR"
	
	icon = "GFX_ship_part_reactor_1"
	icon_frame = 1	
}

component_set = {
	key = "MARA_REACTOR"
	
	icon = "GFX_ship_part_reactor_2"
	icon_frame = 1
}

component_set = {
	key = "WARP_REACTOR"
	
	icon = "GFX_ship_part_reactor_3"
	icon_frame = 1	
}

component_set = {
	key = "ADV_WARP_REACTOR"
	
	icon = "GFX_ship_part_reactor_4"
	icon_frame = 1	
}

component_set = {
	key = "ENRICEHD_REACTOR"
	
	icon = "GFX_ship_part_reactor_5"
	icon_frame = 1	
}

component_set = {
	key = "HYPER_REACTOR"
	
	icon = "GFX_ship_part_reactor_5"
	icon_frame = 1	
}

component_set = {
	key = "SINGULARITY_REACTOR"
	
	icon = "GFX_ship_part_reactor_5"
	icon_frame = 1	
}

component_set = {
	key = "ADV_HYPER_REACTOR"
	
	icon = "GFX_ship_part_reactor_5"
	icon_frame = 1	
}

component_set = {
	key = "ADV_SINGULARITY_REACTOR"
	
	icon = "GFX_ship_part_reactor_5"
	icon_frame = 1	
}
