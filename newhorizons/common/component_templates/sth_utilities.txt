# Icons set here are the ones show in the actual slot. The reason is so that we can support different icons for different sizes. 

#############
#	ARMOR	#
#############

@armor_1S = 1
@armor_1M = 2
@armor_1L = 4

@armor_2S = 2
@armor_2M = 4
@armor_2L = 8

@armor_3S = 3
@armor_3M = 6
@armor_3L = 12

@armor_4S = 4
@armor_4M = 8
@armor_4L = 16

@armor_5S = 5
@armor_5M = 10
@armor_5L = 20

@armor_6S = 6
@armor_6M = 12
@armor_6L = 24

@armor_7S = 7
@armor_7M = 14
@armor_7L = 28

#############
#	COST	#
#############

@cost_1S = 2
@cost_1M = 4
@cost_1L = 8

@cost_2S = 4
@cost_2M = 8
@cost_2L = 16

@cost_3S = 6
@cost_3M = 12
@cost_3L = 24

@cost_4S = 8
@cost_4M = 16
@cost_4L = 32

@cost_5S = 10
@cost_5M = 20
@cost_5L = 40

@cost_6S = 12
@cost_6M = 24
@cost_6L = 48

@cost_7S = 14
@cost_7M = 28
@cost_7L = 56

#############
# UTILITIES #
#############

#Science Ship Parts
utility_component_template = {
	key = "MEDIUM_SCIENCE_LAB_1"
	size = medium
	icon = "GFX_ship_part_science_lab_1"
	icon_frame = 1
	power = 0
	cost = 0
	science_lab_speed = 1
	science_lab_level = 1
	class_restriction = { shipclass_science_ship }

	component_set = "science_labs"
	
	ai_weight = {
		weight = 1
	}
}

#
# Armor
#

# Tier 1a Armor
utility_component_template = {
	key = "SMALL_ARMOR_1a"
	size = small
	icon = "GFX_ship_part_armor_1a"
	icon_frame = 1
	power = 0
	cost = @cost_3S
	armor_value = @armor_3S
	
	modifier = {
		ship_upkeep_mult = 0.03
	}
	
	prerequisites = { "tech_ship_armor_1a" }
	component_set = "ARMOR_1a"
}


# Tier 1b Armor
utility_component_template = {
	key = "SMALL_ARMOR_1b"
	size = small
	icon = "GFX_ship_part_armor_1b"
	icon_frame = 1
	power = 0
	cost = @cost_1S
	armor_value = @armor_1S

	prerequisites = { "tech_ship_armor_1b" }
	component_set = "ARMOR_1b"
}

# Tier 2a Armor
utility_component_template = {
	key = "SMALL_ARMOR_2a"
	size = small
	icon = "GFX_ship_part_armor_2a"
	icon_frame = 1
	power = 0
	cost = @cost_4S
	armor_value = @armor_4S

	modifier = {
		ship_upkeep_mult = 0.03
	}	
	
	prerequisites = { "tech_ship_armor_2a" }
	component_set = "ARMOR_2a"
}

# Tier 2b Armor
utility_component_template = {
	key = "SMALL_ARMOR_2b"
	size = small
	icon = "GFX_ship_part_armor_2b"
	icon_frame = 1
	power = 0
	cost = @cost_2S
	armor_value = @armor_2S
	
	prerequisites = { "tech_ship_armor_2b" }
	component_set = "ARMOR_2b"
}

# Tier 3a Armor
utility_component_template = {
	key = "SMALL_ARMOR_3a"
	size = small
	icon = "GFX_ship_part_armor_3a"
	icon_frame = 1
	power = 0
	cost = @cost_5S
	armor_value = @armor_5S

	modifier = {
		ship_upkeep_mult = 0.03
	}
	
	prerequisites = { "tech_ship_armor_3a" }
	component_set = "ARMOR_3a"
}

# Tier 3b Armor
utility_component_template = {
	key = "SMALL_ARMOR_3b"
	size = small
	icon = "GFX_ship_part_armor_3b"
	icon_frame = 1
	power = 0
	cost = @cost_3S
	armor_value = @armor_3S

	prerequisites = { "tech_ship_armor_3b" }
	component_set = "ARMOR_3b"
}

# Tier 4a Armor
utility_component_template = {
	key = "SMALL_ARMOR_4a"
	size = small
	icon = "GFX_ship_part_armor_4a"
	icon_frame = 1
	power = 0
	cost = @cost_6S
	armor_value = @armor_6S

	modifier = {
		ship_upkeep_mult = 0.03
	}
	
	prerequisites = { "tech_ship_armor_4a" }
	component_set = "ARMOR_4a"
}

# Tier 4b Armor
utility_component_template = {
	key = "SMALL_ARMOR_4b"
	size = small
	icon = "GFX_ship_part_armor_4b"
	icon_frame = 1
	power = 0
	cost = @cost_4S
	armor_value = @armor_4S
	
	prerequisites = { "tech_ship_armor_4b" }
	component_set = "ARMOR_4b"
}

# Tier 5a
utility_component_template = {
	key = "SMALL_ARMOR_5a"
	size = small
	icon = "GFX_ship_part_armor_5a"
	icon_frame = 1
	power = 0
	cost = @cost_7S
	armor_value = @armor_7S

	modifier = {
		ship_upkeep_mult = 0.03
	}
		
	prerequisites = { "tech_ship_armor_5a" }
	component_set = "ARMOR_5a"
}

# Tier 5b
utility_component_template = {
	key = "SMALL_ARMOR_5b"
	size = small
	icon = "GFX_ship_part_armor_5b"
	icon_frame = 1
	power = 0
	cost = @cost_5S
	armor_value = @armor_5S

	prerequisites = { "tech_ship_armor_5b" }
	component_set = "ARMOR_5b"
}

# Crystal-Infused Plating 
utility_component_template = {
	key = "SMALL_CRYSTAL_ARMOR_1"
	size = small
	icon = "GFX_ship_part_crystal_armor_1"
	icon_frame = 1
	power = 0
	cost = @cost_3S
	
	modifier = {
		ship_hitpoints_add = 50
	}
	
	prerequisites = { "tech_crystal_armor_1" }
	component_set = "CRYSTAL_ARMOR_1"
}

utility_component_template = {
	key = "MEDIUM_CRYSTAL_ARMOR_1"
	size = medium
	icon = "GFX_ship_part_crystal_armor_1"
	icon_frame = 1
	power = 0
	cost = @cost_3M
	
	modifier = {
		ship_hitpoints_add = 100
	}
	
	prerequisites = { "tech_crystal_armor_1" }
	component_set = "CRYSTAL_ARMOR_1"
}

utility_component_template = {
	key = "LARGE_CRYSTAL_ARMOR_1"
	size = large
	icon = "GFX_ship_part_crystal_armor_1"
	icon_frame = 1
	power = 0
	cost = @cost_3L
	
	modifier = {
		ship_hitpoints_add = 200
	}
	
	prerequisites = { "tech_crystal_armor_1" }
	component_set = "CRYSTAL_ARMOR_1"
}

# Crystal-Forged Plating 
utility_component_template = {
	key = "SMALL_CRYSTAL_ARMOR_2"
	size = small
	icon = "GFX_ship_part_crystal_armor_2"
	icon_frame = 1
	power = 0
	cost = @cost_5S
	
	modifier = {
		ship_hitpoints_add = 75
	}
	
	prerequisites = { "tech_crystal_armor_2" }
	component_set = "CRYSTAL_ARMOR_2"
}

utility_component_template = {
	key = "MEDIUM_CRYSTAL_ARMOR_2"
	size = medium
	icon = "GFX_ship_part_crystal_armor_2"
	icon_frame = 1
	power = 0
	cost = @cost_5M
	
	modifier = {
		ship_hitpoints_add = 150
	}
	
	prerequisites = { "tech_crystal_armor_2" }
	component_set = "CRYSTAL_ARMOR_2"
}

utility_component_template = {
	key = "LARGE_CRYSTAL_ARMOR_2"
	size = large
	icon = "GFX_ship_part_crystal_armor_2"
	icon_frame = 1
	power = 0
	cost = @cost_5L
	
	modifier = {
		ship_hitpoints_add = 300
	}
	
	prerequisites = { "tech_crystal_armor_2" }
	component_set = "CRYSTAL_ARMOR_2"
}


#Border Extruder
utility_component_template = {
	key = "BORDER_EXTRUDER_1"
	size = medium
	icon = "GFX_ship_part_border_extruder_1"
	icon_frame = 1
	power = 0
	cost = 20
	border_extrusion_range = 15
	class_restriction = { shipclass_outpost_station }
	
	component_set = "border_extruders"
	
	ai_weight = {
		weight = 1
	}
}

utility_component_template = {
	key = "BORDER_EXTRUDER_2"
	size = medium
	icon = "GFX_ship_part_border_extruder_2"
	icon_frame = 1
	power = 0
	cost = 20
	border_extrusion_range = 20
	class_restriction = { shipclass_outpost_station }
	hidden = yes
	
	component_set = "border_extruders"
	
	ai_weight = {
		weight = 2
	}
}

utility_component_template = {
	key = "BORDER_EXTRUDER_3"
	size = medium
	icon = "GFX_ship_part_border_extruder_3"
	icon_frame = 1
	power = 0
	cost = 20
	border_extrusion_range = 25
	class_restriction = { shipclass_outpost_station }
	hidden = yes
	
	component_set = "border_extruders"
	
	ai_weight = {
		weight = 3
	}
}

#
# Utility buffers
#

# EPS Relays
utility_component_template = {
	key = "EPS_RELAY_1"
	size = small
	icon = "GFX_ship_part_eps_1"
	icon_frame = 1
	
	cost = 25
	power = 5
	
	modifier = {
		ship_fire_rate_mult = 0.05
	}
	prerequisites = { "tech_eps_1"}
	component_set = "EPS_RELAY_1"
}

#Coherency Modulator
utility_component_template = {
	key = "MODULATOR_1"
	size = small
	icon = "GFX_ship_part_modulator_1"
	icon_frame =1
	
	cost = 25
	power = -5

	modifier = {
		ship_weapon_range_mult = 0.05
	}
	prerequisites = { "tech_modulation_1" }
	component_set = "MODULATOR_1"
}

# Phase Discriminator
utility_component_template = {
	key = "DISCRIMINATOR_1"
	size = small
	icon = "GFX_ship_part_discriminator_1"
	icon_frame = 1
	
	cost = 25
	power = -5
	
	modifier = {
		weapon_type_energy_weapon_damage_mult = 0.10
	}
	
	prerequisites = { "tech_discriminator_1" }
	component_set = "DISCRIMINATOR_1"
}